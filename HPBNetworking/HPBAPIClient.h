//
//  HPBAPIClient.h
//  HePaiBase
//
//  Created by 张闽·Phoenix on 2016/10/27.
//  Copyright © 2016年 张闽·Phoenix. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "HPBUserManager.h"
typedef void(^HPBHTTPCompletionBlock)(id viewModel);  //请求成功，返回result为1的结果
typedef void(^HPBHTTPFailureBlock)(NSError * error);  //请求失败，放回error
typedef void(^HPBHTTPWarningBlock)(id viewModel);     //请求成功，返回result为0的结果

typedef NS_ENUM(NSInteger, HPBHTTPMethod)
{
    HPBHTTPMethodGET = 0,
    HPBHTTPMethodPOST,
    HPBHTTPMethodPOSTWithMultiMedia,
};

typedef NS_ENUM(NSInteger, HPBMediaType)
{
    HPBMediaTypeImage = 0,
    HPBMediaTypeAudio,
    HPBMediaTypeVideo,
};

@interface HPBMedia : NSObject

@property (assign, nonatomic) HPBMediaType type;
@property (copy, nonatomic) NSArray * medias;
@property (copy, nonatomic) NSString * name;
@property (nonatomic, copy) NSString * fileName;

@end

@interface HPBHTTPEntity : NSObject

@property (assign, nonatomic) HPBHTTPMethod method;
@property (strong, nonatomic) HPBMedia * media;
@property (copy, nonatomic) NSString * path;
@property (copy, nonatomic) NSDictionary * params;
@property (strong, nonatomic) Class targetClass;
@property (nonatomic, assign) BOOL cacheEnable;   //是否缓存请求结果, default is NO.


- (NSString *)configurePath;

@end

@interface HPBAPIClient : AFHTTPSessionManager
    
+ (instancetype)sharedClient;

+ (id)requireWithEntity:(HPBHTTPEntity *)entity
             completion:(HPBHTTPCompletionBlock)completion
                failure:(HPBHTTPFailureBlock)failure;


+ (id)requireWithEntity:(HPBHTTPEntity *)entity
             completion:(HPBHTTPCompletionBlock)completion
                warning:(HPBHTTPWarningBlock)warning
                failure:(HPBHTTPFailureBlock)failure;


    
@end
